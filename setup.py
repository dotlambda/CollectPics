from setuptools import setup

setup(
    name='CollectPics',
    version='1.0',
    long_description=__doc__,
    packages=['collectpics'],
    package_data={'': ['templates/*.html']},
    include_package_data=True,
    zip_safe=False,
    install_requires=['Flask', 'Flask-Dropzone'],
    entry_points={
        'console_scripts': [
            'collectpics = collectpics:main'
        ],
    },
)
