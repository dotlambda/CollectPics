{ lib, buildPythonPackage, fetchPypi, flask }:

buildPythonPackage rec {
  pname = "Flask-Dropzone";
  version = "1.4.3";

  src = fetchPypi {
    inherit pname version;
    sha256 = "f04f04550361eaa497000271ce533896e6df35273d9c95902f92510b794e4c47";
  };

  propagatedBuildInputs = [ flask ];
}
