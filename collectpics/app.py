import os
import argparse
import subprocess
from flask import Flask, render_template, request
from flask_dropzone import Dropzone

app = Flask(__name__)
dropzone = Dropzone(app)

app.config['DROPZONE_SERVE_LOCAL'] = True

app.config['DROPZONE_UPLOAD_MULTIPLE'] = True
app.config['DROPZONE_PARALLEL_UPLOADS'] = 1

app.config['SECRET'] = 'hie9Du4ESh'

@app.route('/' + app.config['SECRET'] + '/', methods=['GET'])
def index():
    return render_template('index.html')

@app.route('/' + app.config['SECRET'] + '/upload', methods=['POST'])
def upload():
    author = '{} <{}>'.format(request.form['name'], request.form['email'])
    print('Upload by {}'.format(author))
    files = []
    for key, f in request.files.items():
            if key.startswith('file'):
                path = os.path.join(app.config['SAVE_PATH'], f.filename)
                try:
                    os.remove(path)
                except FileNotFoundError:
                    pass
                f.save(path)
                files.append(f.filename)
    subprocess.run(['git', 'annex', 'add', '--batch'], cwd=app.config['SAVE_PATH'], input='\n'.join(files), encoding='utf-8')
    msg = 'La Clusaz 2018: Upload by {}'.format(request.form['name'])
    subprocess.run(['git', 'commit', '-m', msg, '--author', author, '--'] + files, cwd=app.config['SAVE_PATH'])
    return 'Uploaded {} files'.format(len(files))

def main():
    parser = argparse.ArgumentParser(description='Run web server for picture uploading.')
    parser.add_argument('save_path', help='where to store the uploaded files')
    parser.add_argument('port', type=int)
    args = parser.parse_args()
    app.config['SAVE_PATH'] = args.save_path
    app.run(port=args.port, threaded=True)
