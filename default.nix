with import <nixpkgs> {
  overlays = [
    (self: super: {
      python3 = super.python3.override {
        packageOverrides = pythonSelf: pythonSuper: {
          flask-dropzone = pythonSelf.callPackage ./flask-dropzone.nix { };
        };
      };
    })
  ];
};

python3.pkgs.buildPythonApplication {
  name = "CollectPics";

  src = ./.;

  propagatedBuildInputs = with python3.pkgs; [ flask-dropzone ];

  doCheck = false;
}
